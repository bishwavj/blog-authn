//how to run
`npm start` 

//program uses port 3000
`http://localhost:3000/`


//how to register
post `http://localhost:3000/api/user/register`
/*Required{name,email,password}*/


//how to login
post `http://localhost:3000/api/user/login`
/*Required{email,password}*/


/*For user authentication send post request with generted token on '/posts' route*/
get `http://localhost:3000/api/user/posts`

//For creating new article
post `http://localhost:3000/api/user/posts/create`
/*Required{title,description,author}*/


//For updating an article
post `http://localhost:3000/api/user/posts/update/:id`
/*Required {article._id}*/

//For deleting an article
post `http://localhost:3000/api/user/posts/delete/:id`
/*Required {article._id}*/

//For reading all articles
get `http://localhost:3000/api/user/posts/readarticles`

//Fetch articles using username
get `http://localhost:3000/api/user/posts/:username`


//To stop server
`ctrl+c`