const mongoose = require('mongoose');

const articleSchema = new mongoose.Schema({
    username:{
        type:String,
        min:6,
        max:20,
        required:true
    },
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,   
    },
    date: {
        type: Date,
        default: Date.now
    },
    author:{
        type:String,
        min:3,
        max:20,
        required:true,
        default:"anonymous"
    }
});

module.exports = mongoose.model('article', articleSchema);