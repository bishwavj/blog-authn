const router = require('express').Router();
const User = require('../model/User');
const jwt = require('jsonwebtoken');
const {registerValidation, loginValidation} = require('../validation');
const bcrypt = require('bcryptjs');

//validation
const Joi = require('@hapi/joi');

const schema = Joi.object({
    username: Joi.string().min(6).required(),
    name: Joi.string().min(6).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
});

router.post('/register', async (req, res) => {

    // //validation before creating a new user
    const {error}= registerValidation(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    //checking if username is available
    const userExist = await User.findOne({username: req.body.username});
    // console.log(userExist);
    if(userExist) return res.send('Username not available!');

    //checking if user already registered
    const emailExist = await User.findOne({email: req.body.email});
    if(emailExist) return res.status(400).send('Email already exist');

    //Hashing password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);


    //create a new user
    const user = new User({
        username: req.body.username,
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword 
    });
    try{
        const savedUser = await user.save();
        res.send({user: user._id});
    } catch (err) {
        res.status(400).send(err);
    }
});

//login
router.post('/login', async (req, res) => {
    const { error } = loginValidation(req.body);

    if(error) return res.status(400).send(error.details[0].message);
    //checking if email exists
        const user = await User.findOne({email: req.body.email});
        if(!user) return res.status(400).send('Email doesnt exist');
        //checking username
        const userExist = await User.findOne({username: req.body.username});
        if(!userExist) return res.status(400).send('Username doesnt exist!');

        //password is correct
        const validPass = await bcrypt.compare(req.body.password, user.password);
        if(!validPass) return res.status(400).send('Invalid Password');

    //create and assign a token
    
    const token = jwt.sign({_id: user._id, email: req.body.email}, 'abcd');
    res.json({_id: user._id, email: req.body.email, token});
    // res.header('auth-token', token).send(token); 
   
});

router.get('/allusers', (req,res) => {

    User.find( 

        function(err, result){

        if(err){
            res.send(err);
        }
        else{
            res.send(result);
        }
    })
}); 


router.post('/delete/:id', async (req,res) => {

    const uid = await User.findOne({_id: req.body._id});
    if(uid) return res.status(400).send('ID not found');
    else deleteUser(req, res);  
});
    
function deleteUser(req, res) {
    
    User.findByIdAndRemove(req.params.id, 

        function(err, result){

        if(err){
            res.send(err);
        }
        else{
            res.send('Deleted!');
        }
    })
}

module.exports = router;
