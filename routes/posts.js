
const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const router = require('express').Router();
const verify = require('./verifyToken');

//message from "/posts"
router.get('/', verify, (req, res) => {
    res.send('User Authenticated! Welcome to the Blog.');
});

//for saving articles
const articleSchema = require('../article/article.model');
const { createIndexes } = require('../article/article.model');

const article = mongoose.model('article', 'articleSchema');

//Route for creating new article
router.post('/create', verify, (req,res) => {
    
    createArticle(req, res);
  
});

//Function for creating new article
function createArticle(req,res) {
    //checking username
   
    var Article = new article({
    username: req.body.username,
    title: req.body.title,
    description: req.body.description,
    author: req.body.author,
    date: req.body.date
    });
    Article.save();
    res.send(Article);
    }

//Route for updating an article
router.post('/update/:id', verify, async (req,res) => {

    const aid = await article.findOne({_id: req.body._id});
    if(aid) return res.status(400).send('ID not found');
    else updateArticle(req, res);  
});

//Function for updating an article
function updateArticle(req, res) {
    
    article.findByIdAndUpdate(req.params.id,
        {'title' : req.body.title,
        'description': req.body.description,
        'author': req.body.author }, {useFindAndModify: false}, 

        function(err, result){

        if(err){
            res.send(err)
        }
        else{
            res.send(result)
        }
    })
}
  
//Delete router
    
router.post('/delete/:id', verify, async (req,res) => {

    const aid = await article.findOne({_id: req.body._id});
    if(aid) return res.status(400).send('ID not found');
    else deleteArticle(req, res);  
});
    
function deleteArticle(req, res) {
    
    article.findByIdAndRemove(req.params.id, 

        function(err, result){

        if(err){
            res.send(err);
        }
        else{
            res.send('Deleted!');
        }
    })
}
     
router.get('/readarticles', verify, (req,res) => {

    article.find( 

        function(err, result){

        if(err){
            res.send(err);
        }
        else{
            res.send(result);
        }
    })
}); 


 
    
router.get('/:usr', verify, async (req,res) => {
    const usr = req.params.usr;
    const uid = await article.find({'username': req.body.usr});
    if(!uid) return res.status(400).send('Username not found');

    else article.find({'username':usr},

        function(err, result){

        if(err){
            res.send(err);
        }
        else{
            res.send(result);
        }
    })
});
    
    
module.exports = router;


