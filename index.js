const express = require('express');
const app = express();
const dotenv = require('dotenv');
const mongoose = require('mongoose');


//import routes
const authRoute = require('./routes/auth');
const postRoute = require('./routes/posts');
const welcomeRoute = require('./routes/welcome');

dotenv.config();
//connect to mongoDB
// mongoose.connect(process.env.DB_CONNECT,
// { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log('connected to DB'));

mongoose.connect(process.env.DB_CONNECT,
    { useNewUrlParser: true, useUnifiedTopology: true });

mongoose.connection
        .once("open", () => console.log("DB Connected"))
        .on("error", error => {
            console.log("Error", error);
        });
    

//middleware
app.use(express.json());
//middleware
app.use('/', welcomeRoute);
app.use('/api/user', authRoute);
app.use('/api/user/posts', postRoute);

app.listen(process.env.PORT|| 3000, () => console.log('server up and running'));
